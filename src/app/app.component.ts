import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Assignment';
  marker = 
  {
  location : "",
  longitude : "",
  latitude : "",
};
submitButtonTitle = "ADD";
  lat: number = 22.719568;
  long: number = 75.857727;
  zoom: number = 8;
  routeLocations = [];
  Locations = [];
  disableShowRoutes = true;
  polyLineVisible  = false;
  formButtonCss = "formAddButton";
  showRouteButtonCss = "showRouteButtonWhite";
//   dummy = [
//     {
//       location : "Indore",
//       longitude : 22.719568,
//       latitude : 75.857727,
//   },
//   {
//     location : "Bhopal",
//     longitude : 23.259933,
//     latitude : 77.412613,
// },
// {
//   location : "Gwalior",
//   longitude : 26.218287,
//   latitude : 78.182831,
// }
//   ];
  onSubmit(){
    if(parseFloat(this.marker.latitude) && parseFloat(this.marker.longitude))
    {
      let loc = {
        location : this.marker.location,
        longitude : parseFloat(this.marker.longitude),
        latitude : parseFloat(this.marker.latitude),
    };
      this.Locations.push(loc);
      this.routeLocations = this.Locations;
      this.marker = 
        {
          location : "",
          longitude : "",
          latitude : "",
      };
    }
    if(this.Locations.length>=2)
    {
      this.disableShowRoutes = false;
      this.showRouteButtonCss = "showRouteButtonBlue";
      this.submitButtonTitle = "SUBMIT";
      this.formButtonCss = "formSubmitButton";
    }
  };

  onShowRouteClicked()
  {
    this.polyLineVisible = true;
  }

  onHomeClicked(){
    this.Locations = [];
    this.disableShowRoutes = true;
    this.showRouteButtonCss = "showRouteButtonWhite";
    this.polyLineVisible = false;
    this.submitButtonTitle = "ADD";
    this.formButtonCss = "formAddButton";
  }
}
